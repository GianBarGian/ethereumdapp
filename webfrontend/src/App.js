import React, { useEffect, useState } from "react";
import { ethers } from 'ethers';

import { CONTACTS_ABI, CONTACTS_ADDRESS } from './contracts_metadata/contacts';

function App() {
  const [contactList, setContactList] = useState();
  const [account, setAccount] = useState();
  const [name, setName] = useState('');
  const [phone, setPhone] = useState(0);
  const [trx, setTrx] = useState({})

  const connectWallet = async () => {
    try {
      const { ethereum } = window;
      if (!ethereum) {
        alert("Please install MetaMask!");
        return;
      }
      
      const contactsTemp = []
      const provider = new ethers.providers.Web3Provider(ethereum)
      const accounts = await provider.send("eth_requestAccounts", []);
      const contactsInstance = new ethers.Contract(CONTACTS_ADDRESS, CONTACTS_ABI, provider);

      const counterBN = await contactsInstance.count()
      const counter = counterBN.toString()

      setAccount(accounts[0])
      
      for (let i = 1; i <= counter; i++) {
        const contact = await contactsInstance.contacts(i)
        contactsTemp.push(contact)
      }
      setContactList(contactsTemp)

    } catch (error) {
      console.log(error);
    }
  };

  const sendContact = async event => {
    event.preventDefault()
    try {
      const { ethereum } = window;

      const provider = new ethers.providers.Web3Provider(ethereum)
      const signer = provider.getSigner()

      const contactsInstance = new ethers.Contract(CONTACTS_ADDRESS, CONTACTS_ABI, signer);
      const trxRes = await contactsInstance.createContact(name, phone);
      setTrx(trxRes)

    } catch (error) {
      console.log(error)
    }
  }

  const getInputName = event => {
    setName(event.target.value)
  }

  const getInputPhone = event => {
    setPhone(event.target.value)
  }

  useEffect(() => {
    connectWallet();
  }, []);


  return (
    <div>
      <p>Your account is: {account}</p>
      <h1>Contacts</h1>
      <ul>
        {contactList && contactList.map(contact => (
          <li key={contact.id.toString()} >
            <h2>{contact.name}</h2>
            <p>{contact.phone}</p>
          </li>
        ))}
      </ul>
      <p>Transaction sent: {trx.hash}</p>
      <form>
        <input type='text' name='name' value={name} onChange={getInputName} placeholder='name' />
        <input type='text' name='phone' value={phone} onChange={getInputPhone} placeholder='phone' />
        <button onClick={sendContact}>Submit</button>
      </form>
    </div>
  )
}

export default App;